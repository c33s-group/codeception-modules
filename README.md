# C33s Codeception Module Collection

This is a collection of multiple codeception modules:

- CarbonDate
- Symfony

## Modules

### CarbonDate

Carbon is an API extension for DateTime which can be used to simulate a date.
This module is its codeception integration.

- https://packagist.org/packages/nesbot/carbon
- https://carbon.nesbot.com/docs/ (Testing Aids)
- https://packagist.org/packages/kylekatarnls/carbonite

```php
final class DevelopmentCest
{
    public function testOnlySpecificVersion(IntegrationTester $I)
    {
        $I->assertNotEquals('2010-05-10 14:00:00', new Carbon());
        $I->simulateCurrentDateAs('2010-05-10 14:00:00');
        $I->assertEquals('2010-05-10 14:00:00', new Carbon());
        $I->stopSimulatingCurrentDate();
        $I->assertNotEquals('2010-05-10 14:00:00', new Carbon());
    }
}
```

### Symfony

Can be used as replacement of codeception/module-symfony (which is extended) by
this module:

```
modules:
    enabled:
        - \C33s\Codeception\Module\Symfony
```

or can be integrated per trait in the Helpers for use with the official Symfony
module.

`tests/_support/Helper/Functional.php`:

```php
use C33s\Codeception\Traits\SymfonyModuleTrait;
use Codeception\Module;

class Functional extends Module
{
    use SymfonyModuleTrait;
    //...
}
```
